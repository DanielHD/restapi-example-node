const { Router } = require('express');
const router = Router();


router.get('/test', (req, res) => {
    //res.send('Hola Dani como estas');
    const data = {
        "name": "Daniel",
        "website": "herdicode.com"
    }
    res.json(data);
    
});

module.exports = router;